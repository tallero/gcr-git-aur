# Maintainer: GT610 <myddz1005@163.com>
# Contributor: Veli Tasalı (tasali) <me@velitasali.com>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>

_pkg="gcr"
_pkgname="gcr-4"
pkgbase="${_pkgname}-git"
pkgname=(
  "${pkgbase}"
  "${_pkgname}-docs-git"
)
pkgver=4.1.0+r14+gc8d2e0f
pkgrel=1
pkgdesc="A library for bits of crypto UI and parsing"
url="https://gitlab.gnome.org/GNOME/${_pkg}"
arch=(x86_64)
license=(GPL2)
depends=(
  glib2
  libgcrypt
  libsecret
  openssh
  p11-kit
  systemd
)
makedepends=(
  gi-docgen
  git
  gobject-introspection
  gtk4
  meson
  vala
)
options=(debug)
source=("git+${url}")
b2sums=('SKIP')

pkgver() {
  cd "${_pkg}"
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd "${_pkg}"
}

build() {
  arch-meson "${_pkg}" build
  meson compile -C build
}

check() {
  # Secure memory tests fail
  dbus-run-session meson test -C build --print-errorlogs || :
}

package_gcr-4-git() {
  provides=(
    "${_pkg}-git"
    "${_pkgname}"
    libgck-2.so
    libgcr-4.so
    libgcr-4-{gtk3,gtk4}.so)
  conflicts=("${_pkgname}")
  backup=("etc/security/limits.d/10-${_pkg}.conf")
  install=gcr.install

  meson install -C build --destdir "${pkgdir}"

  # gcr wants to lock some memory to prevent swapping out private keys
  # https://bugs.archlinux.org/task/32616
  # https://bugzilla.gnome.org/show_bug.cgi?id=688161
  install -Dm644 /dev/stdin "${pkgdir}/${backup}" <<END
@users - memlock 1024
END

  mkdir -p doc/usr/share
  mv {"${pkgdir}",doc}/usr/share/doc
}

package_gcr-4-docs-git() {
  provides=("${_pkgname}-docs")
  conflicts=("${_pkg}-docs")
  pkgdesc+=" (documentation)"
  depends=()

  mv doc/* "$pkgdir"
}

# vim:set sw=2 sts=-1 et:
